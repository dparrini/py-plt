# PyPLT

🇵🇹 🇧🇷 Módulo Python 3 para leitura de arquivos no formato PLT, produzidos pelo programa Anatem.

Python 3 module for reading files in PLT format. These files are produced by the power system dynamic simulation tool Anatem.


## Documentação / Documentation

https://gitlab.com/dparrini/py-plt

## Suporte / Support

Feel free to report any bugs you find. You are welcome to fork and submit pull requests.

## Licença / License

🇵🇹 🇧🇷 Módulo disponível no [GitLab](https://gitlab.com/dparrini/py-plt) sob a licença MIT.

The module is available at [GitLab](https://gitlab.com/dparrini/py-plt) under the MIT license.

