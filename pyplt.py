import math

# enable print
__DEBUG__ = False

# Maximum number of characters per line
__MAX_LINE_CHARS = 80

__VALUE_LENGTH = 12
__MAX_VALUES_PER_FILE_LINE = 6


def read_plt_names(filename):
    """
    Read the series names from a plt file.
    Returns a vector with each series name, starting with
    the time series.
    """
    names = []

    with open(filename, "r") as f:
        names = __read_names(f)

    return names


def read_plt_data(filename):
    """
    Read the series data from a plt file (without series names).
    Returns a matrix where each columns represents a series,
    starting with the time series.
    """
    values = []
    with open(filename, "r") as f:
        linenumber = 0 # current file line number
        # number of variables
        var_count = int(f.readline())
        linenumber = 1

        # skip variable names
        for ivar in range(var_count):
            f.readline()
            linenumber = linenumber + 1

        values = __read_data(f, var_count)
    return values


def read_plt(filename):
    names = []
    data = []
    with open(filename, "r") as file:
        names = __read_names(file)
        data  = __read_data(file, len(names))
    return names, data


def __read_names(file):
    names = []
    # current file line number
    line_number = 0 
    # number of variables
    var_count = int(file.readline())

    names = [""] * var_count
    line_number = 1

    for iseries in range(var_count):
        full_line = file.readline().strip()
        line_len = len(full_line)
        name_len = min([line_len, __MAX_LINE_CHARS])
        names[iseries] = full_line[0:name_len]
        line_number = line_number + 1

    return names


def __read_data(file, var_count):
    # division of values of a timestep in multiple file lines 
    # number of rows per time step
    valrows = math.ceil(var_count / __MAX_VALUES_PER_FILE_LINE)
    # number of cells in the last rows
    lastrow = var_count % __MAX_VALUES_PER_FILE_LINE

    # current timestep value counter 
    ival = 0 
    # values of current timestep
    step_values = [0.0]*var_count 
    # current data row
    ivalrow = 0
    # preallocate series
    values = []
    for line in file:
        cellCount = (ivalrow < valrows - 1) and __MAX_VALUES_PER_FILE_LINE or lastrow
        for icell in range(cellCount):
            ipos = 1 + icell * (1 + __VALUE_LENGTH)
            val_str = line[ipos:ipos + __VALUE_LENGTH]

            value = float(val_str)
            step_values[ival] = value
            ival = ival + 1

        if ival < var_count:
            ivalrow = ivalrow + 1
        else:
            values.append(step_values)
            step_values = [0.0]*var_count
            ivalrow = 0
            ival = 0
    # transpose values
    tsteps = len(values)
    tvalues = [None]*var_count
    # preallocate
    for iseries in range(var_count):
        tvalues[iseries] = [0.0]*tsteps
    # copy from values to tvalues
    for itstep in range(tsteps):
        for iseries in range(var_count):
            tvalues[iseries][itstep] = values[itstep][iseries]
    return tvalues


if __name__ == "__main__":
    import matplotlib.pyplot as plt

    # test reading only variable names
    names = read_plt_names("test.plt")
    print(names)

    # test reading only data
    vals = read_plt_data("test.plt")

    t   = vals[0]
    sig = vals[1]

    plt.figure()
    plt.plot(t, sig)
    plt.xlabel("t (s)")
    plt.show()